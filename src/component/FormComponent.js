import { Component } from "react";

class FormComponent extends Component{
   
    render() {
        return(
            <>
                <div className="row form-group mt-2">
                    <div className="col-4">
                        <label className="label">Firstname </label>
                    </div>
                    <div className="col-8">
                        <input className="form-control" placeholder="your firstname..."/>
                    </div>
                </div>
                <div className="row form-group mt-2">
                    <div className="col-4">
                        <label className="label">Lastname </label>
                    </div>
                    <div className="col-8">
                        <input className="form-control" placeholder="your lastname..."/>
                    </div>
                </div>
                <div className="row form-group mt-2">
                    <div className="col-4">
                        <label className="label">Country </label>
                    </div>
                    <div className="col-8">
                        <select className="form-control">
                            <option>Australia</option>
                        </select>
                    </div>
                </div>
                <div className="row form-group mt-2">
                    <div className="col-4">
                        <label className="label">subject </label>
                    </div>
                    <div className="col-8">
                        <textarea className="form-control" rows='5'></textarea>
                    </div>
                </div>
                <div className="row form-group mt-2">
                    <div className="col-4">
                        <button className="btn btn-success" style={{marginBottom: "5px"}}>Gửi data</button>
                    </div>
                    
                </div>
            </>
        )
    }
}

export default FormComponent;