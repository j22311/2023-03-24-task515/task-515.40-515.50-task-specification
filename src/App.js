import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import TitleComponent from './component/TitleComponent';
import FormComponent from './component/FormComponent';

function App() {
  return (
    <div className="container text-center bg bg-light">
        <TitleComponent/>
        <FormComponent/>
    </div>
  );
}

export default App;
